﻿using AutoMapper;

using Domain.Entities.Entities;
using HR_Portal_API.Models.Request;
using Services.Contracts.DTO;

namespace HR_Portal_API.Mapping
{
    public class SkillMappingsProfile : Profile
    {
        public SkillMappingsProfile()
        {
            CreateMap<Skill, SkillDto>();
        }
    }
}
