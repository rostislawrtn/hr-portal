﻿using AutoMapper;

using Domain.Entities.Entities;
using HR_Portal_API.Models.Request;

using Services.Contracts.DTO;

namespace HR_Portal_API.Mapping
{
    public class CandidateMappingsProfile : Profile
    {
        public CandidateMappingsProfile()
        {
            //CreateMap<Candidate, CandidateDto>().ReverseMap();
            CreateMap<Candidate, CandidateDto>();
            CreateMap<CandidateRequest, CandidateDto>();
            CreateMap<CandidateDto, Candidate>();
        }
    }
}
