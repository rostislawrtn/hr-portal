﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Entities.Entities;
using Infrastructure.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Services.Abstractions;

namespace Services.Impl
{
    public class SkillService :ISkillService
    {
        private readonly IMapper _mapper;
        private DatabaseContext _dbContext;

        public SkillService(
            DatabaseContext context,
            IMapper mapper
            )
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public async Task<Skill> GetByGuid(Guid id)
        {
            Skill? skill = await _dbContext.Skills
                .Where(c => c.Id == id)
                .FirstOrDefaultAsync().ConfigureAwait(true);

            if (skill == null)
            {
                throw new KeyNotFoundException("Skill not found");
            }

            return skill;
        }
    }
}
