﻿namespace Domain.Entities.Entities
{
    public class JobReply : BaseEntity
    {
        public Candidate Candidate { get; set; }
        public Job Job { get; set; }
        public Status Status { get; set; }

    }
}
