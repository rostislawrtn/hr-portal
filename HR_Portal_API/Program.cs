using Domain.Entities.Abstractions.Repositories;

using Infrastructure.EntityFramework;
using Microsoft.AspNetCore.Identity;

using Services.Abstractions;
using Services.Impl;

var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<DatabaseContext>();   // ������������� � ��������� ������ ���������
builder.Services.AddSwaggerGen();                   // ���������� Swagger, ������: https://localhost/swagger/index.html
builder.Services.AddAutoMapper(typeof(Program));    // ���������� AutoMapper ��� ��������� ������ � ���������� "Entities <-> DTO"

// ���������� ������ � ���������� � �����������
builder.Services.AddScoped<ICandidateService, CandidateService>();
builder.Services.AddScoped<IJobService, JobService>();
builder.Services.AddScoped<ISkillService, SkillService>();
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: "react",
        builder =>
        {
            builder.WithOrigins("*").
                    WithHeaders("*").
                    WithMethods("*");
        });
});


builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

var dbInitializer = builder.Services.BuildServiceProvider().GetService<IDbInitializer>();
if (dbInitializer != null)
{
    dbInitializer.InitializeDb();
}

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}

app.UseCors("react");

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.MapGet("/", () => "Service Online!");

app.Run();
