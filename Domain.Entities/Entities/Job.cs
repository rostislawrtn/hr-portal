﻿namespace Domain.Entities.Entities
{
    public class Job : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual IEnumerable<JobReply>? Replies { get; set; }
    }
}
