﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Entities
{
    public class Candidate : BaseEntity
    {

        public string LastName { get; set; }

        public string FirstName { get; set; }
        
        public string Surname { get; set; }

        public string FullName { get; set; }

        public int Age { get; set; }

        public Job? PossibleJob { get; set; }

        public virtual IEnumerable<JobReply>? Replies { get; set; }
    }
}
