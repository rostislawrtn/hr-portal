﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.EntityFramework;
public class DatabaseContext : DbContext
{
    protected readonly IConfiguration _configuration;

    public DatabaseContext()
    {

    }
    
    public DatabaseContext(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public DbSet<Candidate> Candidates { get; set; }

    public DbSet<Job> Jobs { get; set; }

    public DbSet<Skill> Skills { get; set; }

    public DbSet<Status> Statuses { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(_configuration.GetConnectionString("PostgreSQL"));
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Candidate>()
            .HasMany(c => c.Replies)
            .WithOne(c => c.Candidate);

        modelBuilder.Entity<Job>()
            .HasMany(c => c.Replies)
            .WithOne(c => c.Job);

        //modelBuilder.Entity<Job>()
        //    .HasMany(j => j.Skills)
        //    .WithMany(s => s.Jobs);

        //modelBuilder.Entity<Skill>()
        //    .HasMany(j => j.Jobs)
        //    .WithMany(s => s.Skills);

    }
}
