﻿namespace HR_Portal_API.Models.Request.Skill
{
    public class SkillModel
    {
        public Guid Id { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
