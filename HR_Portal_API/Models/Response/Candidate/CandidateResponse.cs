﻿namespace HR_Portal_API.Models.Response.Candidate
{
    public class CandidateResponse
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
    }
}
