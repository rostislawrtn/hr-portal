﻿using HR_Portal_API.Models.Response.Candidate;

namespace HR_Portal_API.Models.Response.Job
{
    public class JobResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual IEnumerable<CandidateResponse> Candidates { get; set; }
    }
}