﻿using AutoMapper;
using Domain.Entities.Entities;

using HR_Portal_API.Models.Request;
using HR_Portal_API.Models.Response.Job;

using Services.Contracts.DTO;

namespace HR_Portal_API.Mapping
{
    public class JobMappingsProfile : Profile
    {
        public JobMappingsProfile()
        {
            CreateMap<JobDto, Job>();
            CreateMap<Job, JobDto>();


            CreateMap<JobRequest, JobDto>();
            CreateMap<JobDto, JobResponse>();
        }
    }
}
