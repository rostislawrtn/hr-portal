﻿using AutoMapper;
using Domain.Entities.Entities;
using Services.Abstractions;
using Services.Contracts.DTO;
using Infrastructure.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Entities.Abstractions.Repositories;

namespace Services.Impl
{
    public class CandidateService : ICandidateService
    {
        private readonly IMapper mapper;
        private readonly IRepository<Candidate> candidateRepository;

        public CandidateService(
            IRepository<Candidate> candidateRepository,
            IMapper mapper
            )
        {
            this.candidateRepository = candidateRepository; 
            this.mapper = mapper;
        }

        public async Task<Candidate> GetByGuid(Guid id)
        {
            Candidate? candidate = await this.candidateRepository.GetByIdAsync(id);

            if (candidate == null)
            {
                throw new KeyNotFoundException("Candidate not found");
            }

            return candidate;
        }

        public async Task<Guid> CreateAsync(CandidateDto obj)
        {
            await this.candidateRepository.AddAsync(this.mapper.Map<Candidate>(obj));

            return obj.Id;
        }
    }
}
