﻿namespace HR_Portal_API.Models.Request
{
    public class JobRequest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
