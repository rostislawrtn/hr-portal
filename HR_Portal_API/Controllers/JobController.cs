﻿using AutoMapper;

using HR_Portal_API.Models.Request;
using HR_Portal_API.Models.Response.Job;

using Microsoft.AspNetCore.Mvc;

using Services.Abstractions;
using Services.Contracts.DTO;

namespace HR_Portal_API.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class JobController : ControllerBase
    {
        private IJobService _service;
        private IMapper _mapper;

        public JobController(IJobService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok((await _service.GetAll()).Select(x=> _mapper.Map<JobResponse>(x)));
        }

        [HttpPost]
        public async Task<IActionResult> Create(JobRequest job)
        {
            try
            {
                return Ok((await _service.CreateAsync(_mapper.Map<JobDto>(job))));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
