﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Entities
{
    public class Skill : BaseEntity
    {

        public string Name { get; set; }

        public string Description { get; set; }

        //public virtual List<Job> Jobs { get; set; }

        //public Skill()
        //{
        //    Id = Guid.NewGuid();
        //}
    }
}
