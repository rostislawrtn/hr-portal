﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities.Entities;

namespace Services.Abstractions
{
    public interface ISkillService
    {
        Task<Skill> GetByGuid(Guid id);
    }
}
