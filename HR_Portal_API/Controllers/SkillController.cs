﻿using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using AutoMapper;
using HR_Portal_API.Models.Request.Skill;

namespace HR_Portal_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SkillController : ControllerBase
    {
        private ISkillService _service;
        private IMapper _mapper;

        public SkillController(ISkillService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{guid}")]
        public async Task<IActionResult> Get(Guid guid)
        {
            return Ok(_mapper.Map<SkillModel>(await _service.GetByGuid(guid)));
        }
    }
}
