﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities.Entities
{
    public class Status : BaseEntity
    {
        public string Description { get; set; }

        public Candidate CandidateID { get; set; }
    }
}
