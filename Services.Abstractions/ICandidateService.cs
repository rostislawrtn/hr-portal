﻿using Domain.Entities.Entities;
using Services.Contracts.DTO;

namespace Services.Abstractions
{
    public interface ICandidateService
    {
        Task<Candidate> GetByGuid(Guid id);
        Task<Guid> CreateAsync(CandidateDto id);

        //Task<IEnumerable<CandidateDto>> GetAllCandidatesAsync();
    }
}