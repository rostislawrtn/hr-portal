﻿using AutoMapper;
using Domain.Entities.Entities;
using Services.Abstractions;
using Services.Contracts.DTO;
using Infrastructure.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Entities.Abstractions.Repositories;

namespace Services.Impl
{
    public class JobService :IJobService
    {
        private readonly IMapper mapper;
        private readonly IRepository<Job> jobRepository;

        public JobService(
            IRepository<Job> jobRepository,
            IMapper mapper)
        {
            this.jobRepository = jobRepository;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<JobDto>> GetAll()
        {
            return (await this.jobRepository.GetAllAsync()).Select(x=> this.mapper.Map<JobDto>(x));
        }

        public async Task<Guid> CreateAsync(JobDto obj)
        {
            await this.jobRepository.AddAsync(this.mapper.Map<Job>(obj));
            return obj.Id;
        }

    }
}

