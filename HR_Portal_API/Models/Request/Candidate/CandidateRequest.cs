﻿using Domain.Entities.Entities;

namespace HR_Portal_API.Models.Request
{
    public class CandidateRequest
    {
        public Guid Id { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string FullName { get; set; }

        public int Age { get; set; }

        public Domain.Entities.Entities.Job? PossibleJob { get; set; }

    }
}
