﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Services.Abstractions;
using AutoMapper;
using Domain.Entities.Entities;
using HR_Portal_API.Models.Request;
using Services.Contracts.DTO;
using HR_Portal_API.Models.Response.Candidate;

namespace HR_Portal_API.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CandidateController : ControllerBase
    {
        private ICandidateService _service;
        private IMapper _mapper;

        public CandidateController(ICandidateService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetById(Guid guid)
        {
            return Ok(_mapper.Map<CandidateResponse>(await _service.GetByGuid(guid)));
        }

        [HttpPost]
        public async Task<IActionResult> Create(CandidateRequest candidate)
        {
            try
            {
                return Ok((await _service.CreateAsync(_mapper.Map<CandidateDto>(candidate))));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}