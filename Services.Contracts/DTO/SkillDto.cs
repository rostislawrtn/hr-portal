﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contracts.DTO
{
    public class SkillDto
    {
        public Guid Id { get; private set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
