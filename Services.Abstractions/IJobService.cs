﻿using Domain.Entities.Entities;
using Services.Contracts.DTO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Abstractions
{
    public interface IJobService
    {
        Task<IEnumerable<JobDto>> GetAll();
        Task<Guid> CreateAsync(JobDto obj);
    }
}
