﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.EntityFramework
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DatabaseContext dataContext;

        public EfDbInitializer(DatabaseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void InitializeDb()
        {
            this.dataContext.Database.EnsureDeleted();
            this.dataContext.Database.EnsureCreated();
        }
    }
}
